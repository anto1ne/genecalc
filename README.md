# GeneCalc

Outils de suivi de la génétique d'un troupeau d'animaux (pedigree, croisement, consanguinité)


Cet outil permet notamment de  
  * faire une liste d’animaux avec leur caractéristique  
  * synthétiser/regrouper ces animaux par caractéristique  
  * analyser le pedigree d’un animal  
  * simuler un croisement entre 2 animaux  
  * estimer la consanguinité d’un ou plusieurs croisements  
